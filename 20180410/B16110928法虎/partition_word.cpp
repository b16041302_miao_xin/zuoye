#include <iostream>

using namespace std;


int isFengefu(int ch)
{
    if(ch==' ' || ch== ',' ||ch=='.' ||ch=='!' ||ch=='?')
        return 1;
    return 0;
}

void partition_word(char* s)
{
    int i=0;
    while(s[i]!='\0')
    {
        while(s[i]!='\0' && isFengefu(s[i])) i++;
        int start=i;
        while(s[i]!='\0' && !isFengefu(s[i])) i++;
        int end=i-1;
        if(start<=end)
        {
            for(int k=start; k<=end; k++)
                cout << s[k];
            cout << endl;
        }
    }
}

int main()
{
    char s[100]=" I  like,,,.. C++ programming!";
    partition_word(s);
    return 0;
}
