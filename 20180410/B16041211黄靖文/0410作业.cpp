//
//  main.cpp
//  0410作业.cpp
//
//  Created by 黄靖文 on 2018/4/10.
//  Copyright © 2018年 黄靖文. All rights reserved.
//  分解句子，得到每个单词，空格、逗号、句号、问号和感叹号作为分隔符
#include <iostream>
#include <stdlib.h>
using namespace std;

int isBD(char c)
{
    if (c == ',')
        return 1;
    if (c == '.')
        return 1;
    if (c == '?')
        return 1;
    if (c == ' ')
        return 1;
    if (c == '!')
        return 1;
    return 0;
}
int main()
{
    int i = 0;
    char s[100] = "I like,,,.. C++ programing";
    while (s[i] != '\0' && s[i] != 0)
        i ++;
    int si = i;
    while (isBD(s[i] == 0) && s[i] != 0)
        i ++;
    int ei = i - 1;
    for (int j = si;j <= ei;j++)
        cout << s[j];
    cout << endl;
}
