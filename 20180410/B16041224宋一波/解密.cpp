#include<iostream>
using namespace std;
char *jiemi(char *s,int i){
	int j;
	char t;
	for(j=0;j<i;j++){					//右移3位 
		if(s[j]<='z'&&s[j]>='a')
			s[j]=(s[j]-'a'+3)%26+'a';
		else if(s[j]<='Z'&&s[j]>='A')
			s[j]=(s[j]-'A'+3)%26+'A';
	}
	for(j=0;j<i/2;j++){					//逆置 
		t=s[i-j-1];
		s[i-j-1]=s[j];
		s[j]=t; 
	} 
	for(j=0;j<i;j++){
		if(s[j]<='Z'&&s[j]>='A')
			s[j]=s[j]+32;
		else if(s[j]<='z'&&s[j]>='a')
			s[j]=s[j]-32;
	}
	return s;
}
int main(){
	int i=0;
	char s[50]="";
	cout<<"请输入密文（长度小于且只包含大小写字母）："<<endl;
	while((s[i]=getchar())!='\n'){
		i++;
	}
	jiemi(s,i);
	cout<<s;
	return 0;
}
