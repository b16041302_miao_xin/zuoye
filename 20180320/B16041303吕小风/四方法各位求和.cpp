#include<stdio.h>
int For(int n)
{
	int s=0;
	for(int i=n;i>0;i/=10)
	{
		s+=i%10;
	}
	return s;
}
int While(int n)
{
	int s=0;
	while(n>0)
	{
		s+=n%10;
		n/=10;
	}
	return s;
}
int Do(int n)
{
	int s=0;
	do
	{
		s+=n%10;
		n/=10;
	}while(n>0);
	return s;
}
int While1(int n)
{
	int s=0;
	while(1)
	{
		s+=n%10;
		n/=10;
		if(n==0)
			break;
	}
	return s;
}
int main()
{
	int n;
	scanf("%d",&n);
	printf("%d,%d,%d,%d",For(n),While(n),Do(n),While1(n));
	return 0;
}