#include<iostream>
using namespace std;
int func1(int x)
{
	int count=0;
	while(x){
		count+=x%10;
		x/=10;
	}
	return count;	
} 

int func2(int x)
{
	int count=0;
	do{
		count+=x%10;
		x/=10;
	}while(x);
	return count;
}

int func3(int x)
{
	int count=0;
	for(int num=x;num;num/=10){
		count+=num%10;
	}
	return count;
}

int func4(int x){
	int count=0;
	while(1){
		count+=x%10;
		x/=10;
		if(x==0){
			break;
		}
		
	}
	return count;
}
int main(){
	int a,x;
	cin>>x;
	a=func1(x);
	//a=func2(x);
	//a=func3(x);
	//a=func4(x);
	cout<<a<<endl;
	return 0;
}
