#include<iostream>
using namespace std;
//for循环 
void f1(int n){
	int sum=0;
	for(int i=0;n>0;i++){
		sum+=n%10;
		n=n/10;
	}
	cout<<sum;
}
//while循环 
void f2(int n){
	int sum=0;
	while(n>0){
		sum+=n%10;
		n=n/10;
	}
	cout<<sum;
} 
//do-while循环
void f3(int n){
	int sum=0;
	do{
		sum+=n%10;
		n=n/10;
	}
	cout<<sum;
} 
//break跳出循环 
void f4(int n){
	int sum=0;
	while(1){
		sum+=n%10;
		n=n/10;
		if(n==0)
			break;
	}
	cout<<sum;
}
