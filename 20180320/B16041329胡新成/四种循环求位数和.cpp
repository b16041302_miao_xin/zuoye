#include<iostream>
using namespace std;
int One(int x)
{
       double sum=0;
       for(;x;x=x/10)
              sum+=x%10;
       return sum;
}
 int Two(int x)
 {
        double sum=0;
        while(x)
        {
               sum+=x%10;
               x=x/10;
        }
        return sum;
 }
 int Three(int x)
 {
        double sum=0;
        do{
              sum+=x%10;
              x=x/10;
        }while(x);
        return sum;
 }
 int Four(int x)
 {
        double sum=0;
        while(1){
              sum+=x%10;
              x=x/10;
              if(!x)
              break;
        }
        return sum;
 }
int main()
{
       cout<<"Input:\n";
       int n;
       cin>>n;
       cout<<One(n)<<'\n';
       cout<<Two(n)<<'\n';
       cout<<Three(n)<<'\n';
       cout<<Four(n)<<'\n';
       return 0;
}
