#include<iostream>
using namespace std;
int Countnum1(int x)
{
	int count=0;
   while(x)
   {
     count+=x%10;
     x/=10;
   }
   return count;
}
int Countnum2(int x)
{
	int count=0;
    do{
	  count+=x%10;
	  x/=10;
	}while(x);
	return count;
}
int Countnum3(int x)
{
    int count=0;
	for(int num=x;num;num/=10)
	   count+=num%10;
	return count;
}
int Countnum4(int x)
{
    int count=0;
	while(1)
	{
		count+=x%10;
		x/=10;
	  if(x==0)
         break;
	}
	return count;
}
int main()
{
	int res,x;
	cin>>x;
	res=Countnum4(x);
	//res=Countnum1(x);
	//res=Countnum2(x);
	//res=Countnum3(x);
	cout<<res<<endl;
  return 0;
}