#include<iostream>
using namespace std;
void transpose(int matrix[4][4])
{

    for (int layer = 0; layer < 4/2; ++layer)
    {
        int first = layer;
        int last = 4 - first - 1;
        for (int i = first; i < last; ++i)
        {
            int tmp = matrix[first][i];
            matrix[first][i] = matrix[last - i + layer][first];
            matrix[last - i + layer][first] = matrix[last][last - i + layer];
            matrix[last][last - i + layer] = matrix[i][last];
            matrix[i][last] = tmp;
        }
    }
}
void printImage(int image[4][4])
{
    for(int i=0; i<4; ++i)
    {
        for(int j=0; j<4; ++j)
        {
            cout<<image[i][j]<<" ";
        }
        cout<<endl;
    }
}
int main(){
    int a[4][4]={{1,2,3,4},{9,10,11,12},{13,9,5,1},{15,11,7,3}};
    printImage(a);
    transpose(a);
    cout<<endl;
    printImage(a);
    return 0;
}
