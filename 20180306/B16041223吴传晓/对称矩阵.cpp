#include<cstdio>
#include<iostream>
#define size 4

using namespace std;

int main() {
	unsigned int x[10][10] = {0};
	unsigned int s = 1; 
	for(int i = 0; i < size; i++)
		for(int j = i; j < size; j++)
			x[i][j] = x[j][i] = s++;
			
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++)
			printf("%5d ", x[i][j]);
		cout << endl;
	}
	return 0;
}
