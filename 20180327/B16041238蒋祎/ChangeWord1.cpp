#include <stdio.h>		// gets()  puts()
using namespace std;
int main()
{
	int i;
	char s[100]="";
	gets(s);	 
	for(i=0; s[i]!='\0'; i++)
	{
		if(s[i]>='a'&&s[i]<='z')
			s[i] = (s[i] -'a' + 2) % 26 + 'a';
		if(s[i]>='A'&&s[i]<='Z')
			s[i] = (s[i] -'A' + 2) % 26 + 'A';
		if(s[i]>='0'&&s[i]<='9')
			s[i] = (s[i] -'0' + 2) % 10 + '0';
	}
	puts(s);	
	return 0;
}
