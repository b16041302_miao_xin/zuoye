#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
       int n=4,m=4;
       int b[m][n];
       int a[m][n];
       cout<<"请输入原始矩阵： "<<endl;
       for(int i=0;i<m;i++)           
              for(int j=0;j<n;j++)
                     cin>>a[i][j];
       for(int i=0;i<n;i++)             
              for(int j=0;j<n;j++)
                     b[j][n-1-i]=a[i][j];
       cout<<"顺时针旋转结果："<<endl;
       for(int i=0;i<m;i++)                     
              for(int j=0;j<n;j++)
              {
                     cout<<setw(3)<<setiosflags(ios::right)<<b[i][j]<<" ";
                     if(j==n-1)
                            cout<<endl;
              }
       for(int i=0;i<n;i++)             
              for(int j=0;j<n;j++)
                     b[n-1-j][i]=a[i][j];
       cout<<"逆时针旋转结果："<<endl;
       for(int i=0;i<m;i++)                    
              for(int j=0;j<n;j++)
              {
                     cout<<setw(3)<<setiosflags(ios::right)<<b[i][j]<<" ";
                     if(j==n-1)
                            cout<<endl;
              }
       return 0;
}
