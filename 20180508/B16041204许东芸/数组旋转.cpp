#include <iostream>
#include <iomanip>
using namespace std;

#define N 4
//将一个二维N*N矩阵顺时针旋转90°
void show(int a[][N])
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            cout << setw(4) << a[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}
void rotate(int a[][N])
{
   
    //逆序：对一维数组而言，将每一行第i个元素与第N-i-1个元素进行交换
    for (int i = 0; i < N; ++i)
    {
        for (int j = N - 1; j >= N / 2; --j) //注意j循环的条件为 j>=N/2
        {
            int temp = 0;
            temp = a[i][j];   //第i行末元素
            a[i][j] = a[i][N - j - 1];
            a[i][N - j - 1] = temp;
        }
    }
    cout << "逆序后得到旋转后的最终结果如下：" << endl;
    show(a);
}
int main(void)
{
    int a[N][N] = {
        {1,2,3,4},
        {9,10,11,12},
        {13,9,5,1},
        {15,11,7,3} };
    cout << "原矩阵如下所示：" << endl;
    show(a);
    rotate(a);
    return 0;
}
