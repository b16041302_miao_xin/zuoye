#include<stdio.h>

char *strcat(char *str1,char *str2)
{
	if(*str1==NULL||*str2==NULL)
	return NULL;
	char *temp=str1;
	while(*temp!='\0')
	temp++;
	while((*temp++=*str2++)!='\0');
	return str1;
}
int main(){
	char str1[10]="hello";
	char str2[10]="world";
	char *str3=strcat(str1,str2);
	printf("%s",str3);
	return 0;
} 
