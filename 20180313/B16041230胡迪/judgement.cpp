#include <iostream>
using namespace std;

void SelectionSort(int a[],int n){
	for (int i=0;i<n;i++){
		int minIndex=i;
		for (int j=i+1;j<n;j++){
			if(a[j]<a[minIndex]){
				minIndex=j;
				swap(a[i],a[minIndex]);
			}
		}
		
	}
}

void judge(int a[],int n){
	SelectionSort(a,n);
	double average=0,sum=0;
	for (int i=1;i<n-1;i++){
		sum+=a[i];
		
	}
	average=sum/(n-2);
	cout<<average;
	
	
}

int main (void){
	int a[]={1,3,2,4,6,5,8,7,9}, n=9;
	judge(a,n);
	
	return 0;
}
